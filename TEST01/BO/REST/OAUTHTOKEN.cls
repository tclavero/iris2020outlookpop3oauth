Class TEST01.BO.REST.OAUTHTOKEN Extends EnsLib.REST.Operation
{

Property Adapter As EnsLib.HTTP.OutboundAdapter;

Method OAUTH(oRequest As TEST01.BO.REST.OAUTHTOKENReq, Output oResponse As TEST01BO.REST.OAUTHTOKENRes) As %Status
{
	
	Set tSC = $System.Status.OK()	
	$$$LOGINFO("OUAUTH")
	try{		
		set tHttpRequest = ##class(%Net.HttpRequest).%New()	
		Set tHttpRequest.Https = 1
		Set tHttpRequest.SSLCheckServerIdentity = 0		
		Set tSC = tHttpRequest.SetHeader("Content-Type","application/x-www-form-urlencoded")
		Set tSC =tHttpRequest.SetHeader("Accept","*/*")
		Set tSC =tHttpRequest.SetHeader("Connection","keep-alive")
		Set tSC =tHttpRequest.SetHeader("Accept-Encoding","gzip, deflate, br")
		
		
		Do tHttpRequest.InsertFormData("grant_type","client_credentials")
		Do tHttpRequest.InsertFormData("client_id","-")
		Do tHttpRequest.InsertFormData("client_secret","-")
		Do tHttpRequest.InsertFormData("scope","https://outlook.office.com/.default")
									
		$$$LOGINFO("ANTES LLAMADA")				        
		set tSC = ..Adapter.SendFormDataArray(.tHttpResponse,"POST",tHttpRequest,,,..Adapter.URL)		
		Set:$$$ISERR(tSC)&&$IsObject(tHttpResponse)&&$IsObject(tHttpResponse.Data)&&tHttpResponse.Data.Size tSC=$$$ERROR($$$EnsErrGeneral,$$$StatusDisplayString(tSC)_":"_tHttpResponse.Data.Read())		
		$$$LOGINFO("DESPUES LLAMADA")
		quit:$$$ISERR(tSC)
		
				
		set tProxy = ##class(%ZEN.proxyObject).%New()		
		set tSC = ..JSONStreamToObject(tHttpResponse.Data, .tProxy)	
		quit:$$$ISERR(tSC)											
		
		
		$$$LOGINFO("tProxy.access_token : "_tProxy."access_token")
		set ^oAuthToken("token")=tProxy."access_token"
		set ^oAuthToken("error")=0
	} 	catch(ex) {
		set tSC = ex.AsStatus()
		$$$TRACE("tSC error = "_tSC)
	}
	quit tSC
}

XData MessageMap
{
<MapItems>
	<MapItem MessageType="TEST01.BO.REST.OAUTHTOKENReq"> 
		<Method>OAUTH</Method>
	</MapItem>
	</MapItems>
}

}
