Include Ensemble

Class TEST01.Email.Service Extends Ens.BusinessService [ Language = objectscript ]
{

Parameter ADAPTER = "TEST01.Email.InboundAdapter";

Method OnTask() As %Status
{
	if $DATA(^oAuthToken("error"))
	{
		set oReq = ##class(PCI.BO.REST.OAUTHTOKENReq).%New()
		do ..SendRequestSync("TEST01 REST OAUTH OPERATION",oReq,.Res)
	}
	
	if $GET(^oAuthToken("error"),0)
	{
		set oReq = ##class(PCI.BO.REST.OAUTHTOKENReq).%New()
		do ..SendRequestSync("TEST01 REST OAUTH OPERATION",oReq,.Res)
	}
	quit ##super()
}

Method OnProcessInput(pInput As %Net.MailMessage, Output pOutput As %RegisteredObject) As %Status
{
	set tSC = $$$OK
	try
	{
		set vFilenames="" 
		set vCantArchivos=0
		for i=1:1:pInput.Parts.Count()
		{
			$$$LOGINFO(pInput.Parts.GetAt(i))		
		}
		quit:('tSC)	
	}
	Catch (oException)
	{
		set tSC = oException.AsStatus()
	}
	set tSC = $$$ERROR(5001,"Prueba de Lectura")
	Quit tSC
}

}
